// Allows us to perform CRUD operation in a given URL
// Accepts two arguments: URL (required) and options (optional)
// options is used when the dev needs the request body

let posts = []
fetch('https://jsonplaceholder.typicode.com/posts')
    .then(response => response.json())
    .then(data => {
        posts = data
        showPost(posts)
    })

// Add Post
document.querySelector("#form-add-post").addEventListener('submit', event => {
	event.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: "POST",
		body: JSON.stringify({
			title:document.querySelector("#txt-title").value,
			body:document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers: { "Content-Type": "application/json; charset=UTF-8" }
	})
	.then((response) => response.json())
	.then(data => {
		document.querySelector('#txt-title').value = null
		document.querySelector('#txt-body').value = null
        console.log(data)
        showPost([data])
	})
})

const showPost = posts => {
    let postEntries = ""
    posts.forEach(post => {
        postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
    })

    document.querySelector('#div-post-entries').innerHTML = postEntries
}

const editPost = id => {
    const title = document.querySelector(`#post-title-${id}`).innerHTML
    const body = document.querySelector(`#post-body-${id}`).innerHTML

    document.querySelector('#txt-edit-id').value = id
    document.querySelector('#txt-edit-title').value = title
    document.querySelector('#txt-edit-body').value = body

    document.querySelector('#btn-submit-update').removeAttribute('disabled')
}

document.querySelector('#form-edit-post').addEventListener('submit', event => {
    event.preventDefault()
    const postId = document.querySelector('#txt-edit-id').value
    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
        method: "PUT",
        body: JSON.stringify({
            id: postId,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value
        }),
        headers: { "Content-Type": "application/json; charset=UTF-8" }
    }).then(response => response.json()).then(data => {
        console.log(data)
    })

    document.querySelector('#txt-edit-id').value = null
    document.querySelector('#txt-edit-title').value = null
	document.querySelector('#txt-edit-body').value = null
    document.querySelector('#btn-submit-update').setAttribute('disabled', true)
})

// S44 activity: delete a particular post
const deletePost = id => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, { method: "DELETE" })
    // document.querySelector(`#post-${id}`).remove()
    id = Number(id)
    posts = posts.filter(post => post.id !== id)
    showPost(posts)
}